package si.uni_lj.fri.pbd.pbd2020_lab_3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.ListFragment;

public class TitlesFragment extends ListFragment {
    private boolean mDualPane;
    private int mCurCheckPosition = 0;
    @Override
    public void onActivityCreated(Bundle savedState) {
        super.onActivityCreated(savedState);
        // Populate list with our static array of titles.
        setListAdapter(new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.titles)));

        // Check to see if we have a frame in which to embed the details
        // fragment directly in the containing UI.
        View detailsFrame = getActivity().findViewById(R.id.details);
        mDualPane = detailsFrame != null && detailsFrame.getVisibility() ==
                View.VISIBLE;
        if (mDualPane) {
          // In dual-pane mode, list view highlights selected item.
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
          // Make sure our UI is in the correct state.
            showDetails(mCurCheckPosition);
        }

    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        showDetails(position);
    }
    void showDetails(int index) {
        mCurCheckPosition = index;
        if (mDualPane) {
            // We can display everything in-place with fragments.
            // Have the list highlight this item and show the data.
            getListView().setItemChecked(index, true);
            //Make new fragment to show this selection.
            DetailsFragment details = DetailsFragment.newInstance(index);
            // TODO: here you should replace the existing fragment in
           // “details” FrameLayout with the new fragment.
           // hint: use FragmentManager, don’t forget to commit!
            FragmentTransaction item = getFragmentManager()
                    .beginTransaction();
            item.replace(R.id.details, details);
            item.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            item.commit();
        } else {
            // Otherwise we need to launch a new activity to display
            // the details fragment with selected text.
            Intent intent = new Intent();
            intent.setClass(getActivity(), DetailsActivity.class);
            intent.putExtra("index", index);
            startActivity(intent);
        }
    }

}
